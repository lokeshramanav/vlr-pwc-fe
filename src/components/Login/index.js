import React,  { useState } from 'react';
import { Redirect } from "react-router-dom";
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import {useStyles} from './styles';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {login} from '../../services/authServices'


export default function Login() {
  const classes = useStyles();

  const [userData, setUserData] = useState({
    email: "",
    password: "",
    redirectUserToDashboard: false
  });


  const handleChange = name => event => {
    setUserData({ ...userData,[name]: event.target.value });
  };

  const authenticateUser = async(event) => {
    event.preventDefault();

    var data = await login(userData.email , userData.password);
    if(data.success){
        
        localStorage.setItem("accessToken", data.token);
        localStorage.setItem("role", data.role);
        setUserData({ ...userData,redirectUserToDashboard: true });
    }else{
        toast.error(data.error)
    }
   };
  
   const redirectDashboard = () => {
    if (userData.redirectUserToDashboard === true) {
            return <Redirect to="/dashboard" />;
         }
    };

    
  

  return (<div>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={handleChange("email")}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={handleChange("password")}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={authenticateUser}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="/forgot-password" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
                <Link href="/signup" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
          </Grid>
        </form>
      </div>
      <ToastContainer/>
     
    </Container>
     {redirectDashboard()}</div>
  );
}