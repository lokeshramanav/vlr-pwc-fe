import React , {useEffect, useState} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Link from '@material-ui/core/Link';
import {useStyles} from "./styles"
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { getHawkerCentres  , getStallsByHawkerId} from "../../services/hawkerService"
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import { Grid } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import TimePicker from 'rc-time-picker';
import {displayTimeArray} from './displayTime'
export default function Dashboard() {
  const classes = useStyles();

  const [state, setState] = useState({  
    left: false,
  });

  const [hawkerList , setHawkerList] = useState([])
  const [hawker , setHawker] = useState("")
  const [stalls , setStalls] = useState([])
  const [stall , setStall] = useState("")
  const [reservationDateTime, setReservationDateTime] = React.useState(new Date());
  const [displayTimeList , setDisplayTimeList] = useState([])
  const [time , setTime] = useState("")


  const onHawkerSelection = async(event)=>{
    event.preventDefault();
    setHawker(event.target.value);
    let stalls = await getStallsByHawkerId(event.target.value);
    setStalls(stalls.data)
  }

  const onStallSelection = async(event)=>{
    event.preventDefault();
    setStall(event.target.value);
  }

  const handleSetTime = async(event)=>{
    event.preventDefault();
    setTime(event.target.value);
  }


  const init = ()=>{
    async function getHawkerList(){
      let hawkerData = await getHawkerCentres()
      console.log("hawkerData Dashboard" , hawkerData)
      return hawkerData
    }
    getHawkerList().then(data=>
      setHawkerList(data.data)
    )
  }

  useEffect(()=>{
    init()      
  } , [])


  const list = (anchor) => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
          <ListItem button key="Reservations">           
            <ListItemText primary="Reservations"/>
          </ListItem>
      </List>
    </div>
  );
  
  const logoutUser = ()=>{
    localStorage.removeItem("accessToken");
    localStorage.removeItem("role");
  }

  const toggleDrawer = (anchor, open) => (event) => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };

  const handleDateChange = (event)=>{
    event.preventDefault()
    setReservationDateTime(event.target.value)
    console.log(event.target.value)
    setDisplayTimeList(displayTimeArray(event.target.value))
  }

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={toggleDrawer('left', true)}>
            <MenuIcon />
          </IconButton>
          <SwipeableDrawer
            anchor={'left'}
            open={state['left']}
            onClose={toggleDrawer('left', false)}
            onOpen={toggleDrawer('left', true)}
          >
            {list('left')}
          </SwipeableDrawer>
          <Typography variant="h6" className={classes.title}>
            Hawker Buzz
          </Typography>
          <Button color="inherit">
          <Link href="/" color="inherit">
                Ordered Food              
              </Link>
            </Button>
          <Button color="inherit"   onClick={logoutUser}> 
          <Link href="/" color="inherit">
                Logout     
           </Link>       
          </Button>
        </Toolbar>
      </AppBar>
      <Grid container spacing={3}>
      <Grid item xs={12}>
          <Paper className={classes.paper}>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-helper-label">Select Hawker Centre</InputLabel>
                <Select labelId="demo-simple-select-helper-label"          id="demo-simple-select-helper"
                        onChange={onHawkerSelection}
                        value={hawker}
                >     
            <MenuItem value="">
            </MenuItem>
          {
            hawkerList.map((hawker , index )=>{
              return (<MenuItem key={index} value={hawker._id}>{hawker.name}</MenuItem>)
            })
          }
              </Select>
          </FormControl>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-helper-label">Select Stall</InputLabel>
                <Select labelId="demo-simple-select-helper-label"          id="demo-simple-select-helper"
                        onChange={onStallSelection}
                        value={stall}
                >     
            <MenuItem value="">
            </MenuItem>
          {
            stalls.map((stall , index )=>{
              return (<MenuItem key={index} value={stall._id}>{stall.stallName}</MenuItem>)
            })
          }
              </Select>
          </FormControl>
          <FormControl className={classes.formControl} noValidate>
  <TextField
    id="date"
    label="Reservation Date"
    type="date"
    values={reservationDateTime}
    className={classes.textField}
    onChange={handleDateChange}
    InputLabelProps={{
      shrink: true,
    }}
  />
      </FormControl>
      <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-helper-label">Time</InputLabel>
            <Select labelId="demo-simple-select-helper-label"          id="demo-simple-select-helper"
                        onChange={handleSetTime}
                        value={time}
                >     
            <MenuItem value="">
            </MenuItem>
          {
            displayTimeList.map((displayTime , index )=>{
              return (<MenuItem key={index} value={displayTime}>{displayTime}</MenuItem>)
            })
          }
              </Select>
      </FormControl>
      <FormControl className={classes.formControl}>
      <Button variant="contained" color="primary" href="#contained-buttons">
       GET MENU
      </Button>
      </FormControl>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}