import moment from 'moment-timezone';

export const displayTimeArray = (inputDate)=>{
    console.log()

    console.log(inputDate === moment().format('YYYY-MM-DD'))
    var displayTimeArray = []
    var currentDate = moment().format('YYYY-MM-DD')
    
    if(currentDate == inputDate ){
        var now = moment()
        var minuteReminder = 15 - now.minute() % 15
        now.add(minuteReminder, 'minutes');
        while(now < moment().set({hours:23 , minutes:59})){
          
            displayTimeArray.push(now.format('hh:mm a'))
            now.add(15,'minutes')
        }
        
    }else { 
     
        var futureDate = moment(`${inputDate} 00:00`)
      
        while(futureDate < moment(`${inputDate} 00:00`).set({hours:23 , minutes:59})){
            
            displayTimeArray.push(futureDate.format('hh:mm a'))
            futureDate.add(15,'minutes')
        }

    }
    return displayTimeArray
}