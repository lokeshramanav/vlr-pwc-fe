import React , {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { forgotPassword } from '../services/authServices'
const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));


export default function ForgotPwd() {
    const classes = useStyles();

    const [userData, setUserData] = useState({
        email: ""
      });    
    
    const handleChange = name => event => {
      setUserData({ ...userData,[name]: event.target.value });
    };

    const sendResetPwdLink = async(event)=>{
        event.preventDefault();
        try{
            await forgotPassword(userData.email)
            toast.success(`Reset Link sent to ${userData.email}`)
        }catch(error){
            toast.error("Provided email is not a registered email address")
        }
    }

    return(
        <Container component="main" maxWidth="sm">
        <CssBaseline />
        <Typography component="h1" variant="h5">
          Provide your registered email to receive the link to reset password
        </Typography>
        <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={handleChange("email")}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={sendResetPwdLink}
          >
            Send Registration Link
          </Button>
        <ToastContainer/>
        </Container>
    )
}