import React , {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useLocation } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { resetPassword } from "../services/authServices";
import { Redirect } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

export default function ResetPwd() {
    const classes = useStyles();

    const [userData, setUserData] = useState({
        password: ""
    });
    const [redirectToLogin , setRedirectToLogin] = useState(false)

    const search = useLocation().search;
    const resetToken = new URLSearchParams(search).get('reset_token');

    const handleChange = name => event => {
        setUserData({ ...userData,[name]: event.target.value });
    };

    const resetPwd = async(event)=>{
        event.preventDefault();
        try{
            await resetPassword(resetToken , userData.password);
            toast.success("Password Updated")
            setTimeout(function(){ setRedirectToLogin(true) }, 3000);
            
        }catch(error){
            toast.error(error.message);
        }   

    }

    const redirectLogin = ()=>{
        if (redirectToLogin === true) {
            return <Redirect to="/login" />;
         }
    }

    return(
        <div>
        <Container component="main" maxWidth="xs">
             <CssBaseline />
             <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="New Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={handleChange("password")}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password 2"
            label="Confirm Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={resetPwd}
          >
            Reset Password
          </Button>
          <ToastContainer/>
        </Container>
        {redirectLogin()}
        </div>
    );
}