import React , {useState , useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import { getHawkerCentres , getStallsByHawkerId } from '../../services/hawkerService'
import { getSignedUrl , uploadImageToS3 } from "../../services/uploadS3"
import { createDish } from "../../services/dishService"
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 350,
    },
    selectEmpty: {
      marginTop: theme.spacing(10)
    },
    root: {
        flexGrow: 1,
      },
      paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
      },
      image: {
        width: 128,
        height: 128,
      },
      img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
      }
}));
  

export default function AddDish(){
    const classes = useStyles();
    const [hawkerId , setHawkerId] = useState("")
    const [hawkerList , setHawkerList] = useState([])
    const [stallId , setStallId] = useState([])
    const [stallList , setStallList] = useState([])
    const [file , setFile] = useState({file:null , message:''})
    const [dishName , setDishName] = useState("")
    const [unitRate , setUnitRate] = useState("")
    const init = ()=>{

      async function getHawkerList(){
        let hawkerData = await getHawkerCentres()
        console.log("hawkerData" , hawkerData)
        return hawkerData
      }
      getHawkerList().then(data=>
        setHawkerList(data.data)
      )
    }
    useEffect(()=>{
      init()      
    } , []);

    const onHawkerSelection = async(event)=>{
      event.preventDefault();    
      setHawkerId(event.target.value)
      let stallList = await getStallsByHawkerId(event.target.value)
      setStallList(stallList.data)
    }

    const onStallSelection = async(event)=>{
      event.preventDefault();   
      setStallId(event.target.value)
    }
    const addDishName = (event)=>{
      event.preventDefault()
      setDishName(event.target.value)
    }
    const addUnitRate = (event)=>{
      event.preventDefault()
      setUnitRate(event.target.value)
    }

    const getImage = async(event)=>{
      event.preventDefault()
      setFile({...file ,message: "Image Uploading ..." })
      setFile({...file , file: event.target.files})
    }

    const addDishToStall = async(event)=>{
      console.log("File" , file);
      let urlData = await getSignedUrl(file.file[0].name , file.file[0].type)
      console.log(urlData);
      let data = await uploadImageToS3(urlData.signedUrl , file.file[0] , file.file[0].type)
      console.log("THis is inside data" , data);
      let postDishData= await createDish(dishName , unitRate , file.file[0].name ,stallId )
    }
    return (
        <div className={classes.root}>
             <Paper className={classes.paper}>
                <Grid container spacing={2}>
                    <Grid item>
                        <FormControl className={classes.formControl}>
                            <InputLabel id="demo-simple-select-helper-label">Select Hawker Centre</InputLabel>
        <Select labelId="demo-simple-select-helper-label"          id="demo-simple-select-helper"
          onChange={onHawkerSelection}
          value={hawkerId}
        >
          <MenuItem value="">
          </MenuItem>
          {
            hawkerList.map((hawker , index )=>{
              return (<MenuItem key={index} value={hawker._id}>{hawker.name}</MenuItem>)
            })
          }
        </Select>
        </FormControl>
        { <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-helper-label">Select Stall</InputLabel>
        <Select labelId="demo-simple-select-helper-label"          id="demo-simple-select-helper"
          value={stallId}
          onChange={onStallSelection}
        >
          <MenuItem value="">
          </MenuItem>
          {stallList&&
            stallList.map((stall , index )=>{
              return (<MenuItem key={index} value={stall._id}>{stall.stallName}</MenuItem>)
            })
          }
        </Select>
      </FormControl> }
          </Grid>
          <TextField  className={classes.formControl} label="Dish Name" onChange={addDishName}/>
          <TextField  className={classes.formControl} label="Unit Rate" onChange={addUnitRate}/>
          <Input
          className={classes.formControl}
          id='upload-image'
          type='file'
          accept='image/*'
          onChange={getImage}
        />
     
         {file.message!== '' && <h3>{file.message}</h3>}
         <Grid  className={classes.formControl}>
         <Button variant="contained" color="primary" onClick={addDishToStall}>
          Submit
          </Button>
          </Grid>
          </Grid>
      </Paper>
    </div>
    
    )
}