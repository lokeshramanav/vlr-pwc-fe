import React ,  { useState } from 'react';
import { Redirect } from "react-router-dom";
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { useStyles } from "./styles"
import { registerUser } from "../../services/authServices" ;
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function SignUp() {
  const classes = useStyles();

  const [userData, setUserData] = useState({
    email: "",
    password: "",
    firstName:"",
    lastName:"",
    redirectToLogin: false
  });

  const handleChange = name => event => {
    setUserData({ ...userData,[name]: event.target.value });
  };

  const submitUserDetails = async (event) => {
    event.preventDefault();
    var userName = `${userData.firstName} ${userData.lastName}`
    let registeredUserResponse = await registerUser(userName , userData.email , userData.password)
    console.log("registeredUserResponse" , registeredUserResponse);

    registeredUserResponse.success ? setUserData({ ...userData,redirectToLogin: true }):  toast.error(registeredUserResponse.error)
    
  };

  const redirectUser = () => {
    if (userData.redirectToLogin === true) {
            return <Redirect to="/login" />;
         }
    };

  return (
      <div>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="First Name"
                autoFocus
                onChange={handleChange("firstName")}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastName"
                autoComplete="lname"
                onChange={handleChange("lastName")}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={handleChange("email")}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={handleChange("password")}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={submitUserDetails}
          >
            Sign Up
          </Button>
        </form>
      </div>
    </Container>
    <ToastContainer/>
    {redirectUser()}
    </div>
  );
}