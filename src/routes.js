import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import PrivateUserRoute from "./protectedRoutes/PrivateUserRoute"
//components
import SignUp from "./components/SignUp"
import Login from "./components/Login"
import Dashboard from "./components/dashboard"
import AddDish from "./components/AddDish"
import ForgotPwd from "./components/ForgotPassword"
import ResetPwd from "./components/ResetPassword"

const Routes = () => {
    return (
        <BrowserRouter>
        <Switch>
                <Route path="/signup" exact component={SignUp} />
                <Route path="/login" exact component={Login} />
                <Route path="/" exact component={Login} />
                <Route path="/forgot-password" exact component={ForgotPwd} />
                <Route path="/passwordreset" exact component={ResetPwd} />
                <Route path="/add-dish" exact component={AddDish} />
                <PrivateUserRoute path="/dashboard" exact component={Dashboard}/>
        </Switch>
        </BrowserRouter>
    )
}

export default Routes;
