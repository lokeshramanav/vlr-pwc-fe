import axios from "axios";
import config from "../config"
const apiEndpoint = config.apiEndpoint

export const createDish = async(dishName ,unitRate , imageUrlName , stall)=>{

    const config = {
        header: {
          "Content-Type": "application/json",
        }
      };
    
      try{
        const  { data } = await axios.post(
            `${apiEndpoint}/api/dish`,
            {
                dishName : dishName ,
                unitRate : unitRate ,
                imageUrlName: imageUrlName ,
                stall : stall
            },
            config
          );
        console.log("Response" , data); 
        return data;
      }
      catch(err) {
        return err.response.data
      }
}