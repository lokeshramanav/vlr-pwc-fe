import axios from "axios";
import config from "../config"
const apiEndpoint = config.apiEndpoint

export const getSignedUrl = async(key , contentType)=>{

    const config = {
        header: {
          "Content-Type": "application/json",
        }
      };
    
    try{
        const  { data } = await axios.post(
            `${apiEndpoint}/api/s3/generate-put-url`,
            {
                Key:key ,
                ContentType : contentType
            },
            config
          );
        console.log("Response" , data); 
        return data;

    }catch(err){
        console.log("Error Message" , err.response.data.error)
        return err.response.data
    }   
}

export const uploadImageToS3 = async(putUrl , file , contentType)=>{

    const options = {
        params: {
          Key: file.name,
          ContentType: contentType
        },
        headers: {
          'Content-Type': contentType,
          'Access-Control-Allow-Origin': '*'
        }
      };
    try{
        let data = await axios.put(putUrl , file , options) 
        return data;
    }
    catch(err){
        console.log("Upload err" , err.message)
    }

}


