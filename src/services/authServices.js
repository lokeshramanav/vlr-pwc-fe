import axios from "axios";
import config from "../config"
const apiEndpoint = config.apiEndpoint

export const registerUser = async(username , email , password ) =>{

    const config = {
        header: {
          "Content-Type": "application/json",
        }
      };
    
    try{
        const  { data } = await axios.post(
            `${apiEndpoint}/api/auth/register`,
            {
              username,
              email,
              password,
            },
            config
          );
        console.log("Response" , data); 
        return data;

    }catch(err){
        console.log("Error Message" , err.response.data.error)
        return err.response.data
    }   
}

export const login = async(email , password) => {
  const config = {
    header: {
      "Content-Type": "application/json",
    },
  };
  try {
    const { data } = await axios.post(
      `${apiEndpoint}/api/auth/login`,
      { email, password },
      config
    );
    return data;
  }catch(err){
    return err.response.data
  }
}

export const forgotPassword = async(email)=>{
  const config = {
    header: {
      "Content-Type": "application/json",
    },
  };
  try {
    const { data } = await axios.post(
      `${apiEndpoint}/api/auth/forgot-password`,
      { email },
      config
    );
    return data;
  }catch(err){
    return err.response.data
  }
}

export const resetPassword = async(resetToken , password)=>{
  const config = {
    header: {
      "Content-Type": "application/json",
    },
  };
  try {
    const { data } = await axios.put(
      `${apiEndpoint}/api/auth/passwordreset/${resetToken}`,
      { password },
      config
    );
    return data;
  }catch(err){
    return err.response.data
  }

}