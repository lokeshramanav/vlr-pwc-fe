import axios from "axios";
import config from "../config"
const apiEndpoint = config.apiEndpoint

export const getHawkerCentres = async()=>{
    
    const config = {
        header: {
          "Content-Type": "application/json",
        }
    }
    try{
        const  { data } = await axios.get(
            `${apiEndpoint}/api/hawker`,
             config
          );
        console.log("Response" , data); 
        return data;

    }catch(err){
        console.log("Error Message" , err.response.data.error)
        return err.response.data
    } 
    
}

export const getStallsByHawkerId = async(hawkerId)=>{
    console.log("Stall api" , hawkerId)
    const config = {
        header: {
          "Content-Type": "application/json",
        }
      };
    
    try{
        const  { data } = await axios.post(
            `${apiEndpoint}/api/stall/get-list`,
            {
                hawker:hawkerId
            },
            config
          );
        console.log("Response" , data); 
        return data;

    }catch(err){
        console.log("Error Message" , err.response.data.error)
        return err.response.data
    }   
}